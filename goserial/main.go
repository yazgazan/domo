
package main

import (
  "io"
  "fmt"
  "log"

  "github.com/tarm/goserial"
)

func main() {
  fmt.Println("hi !")
  c := &serial.Config{Name: "/dev/ttyACM0", Baud: 115200}
  s, err := serial.OpenPort(c)
  if err != nil {
    log.Fatal(err)
  }
  for {
    buf := make([]byte, 128)
    n, err := s.Read(buf)
    if (err != nil) && (err != io.EOF) {
      log.Fatal(err)
      break
    } else if err == nil {
      log.Printf("%s\n", string(buf[:n]))
    }
  }
}


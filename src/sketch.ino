
#include  <DmxMaster.h>

#include "relays.h"
#include "inputs.h"
#include "commands.h"
#include "serial.h"

void setup()
{
  Serial.begin(115200);

  relays_init();
  inputs_init();
}

void loop()
{
  static int i = 0;

  delay(100);
  inputs_update();
  commands_process();
  relays_update();
  process_serial();
  ++i;
}

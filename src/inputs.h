
#ifndef   INPUTS_H_
# define  INPUTS_H_

typedef struct s_Input {
  int pin;
  int base_state;
  int state;
  struct s_Input * next;
} Input;

void inputs_init(void);
void inputs_update(void);
Input const * input_get(int id);
int input_get_state(int id);
void inputs_reset(void);

#endif


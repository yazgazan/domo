
#ifndef   RELAYS_H_
# define  RELAYS_H_

typedef struct {
  int id;
  int state;
} Relay;

void relays_reset(void);
void relays_init(void);
void relays_update(void);
void relay_set_state(int id, int state);
int relay_get_state(int id);

#endif


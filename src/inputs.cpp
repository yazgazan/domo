
#include "Arduino.h"

#include "inputs.h"

#include "globals.h"

static inline int input_read(int id)
{
  return digitalRead(g_inputs[id].pin);
}

static int input_read_state(int id)
{
  const int val = input_read(id);

  if (g_inputs[id].base_state == 1)
  {
    return !val;
  }
  return val;
}

void inputs_reset(void)
{
  for (int i = 0; g_inputs[i].pin != -1; ++i)
  {
    g_inputs[i].base_state = input_read(i);
    g_inputs[i].state = input_read_state(i);
  }
}

void inputs_init(void)
{
  for (int i = 0; g_inputs[i].pin != -1; ++i)
  {
    pinMode(g_inputs[i].pin, INPUT_PULLUP);
    delay(20);
    g_inputs[i].base_state = input_read(i);
    g_inputs[i].state = input_read_state(i);
  }
}

void inputs_update(void)
{
  for (int i = 0; g_inputs[i].pin != -1; ++i)
  {
    g_inputs[i].state = input_read_state(i);
  }
}

Input const * input_get(int id)
{
  return &(g_inputs[id]);
}

int input_get_state(int id)
{
  return g_inputs[id].state;
}


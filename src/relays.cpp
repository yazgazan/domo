
#include  <Arduino.h>
#include  <DmxMaster.h>

#include "relays.h"

#include "globals.h"

static int relays_count(void)
{
  int i;

  for (i = 0; g_relays[i].id != -1; ++i)
    ;
  return i;
}

void relays_reset(void)
{
  for (int i = 0; g_relays[i].id != -1; ++i)
  {
    g_relays[i].state = 0;
  }
}

void relays_init(void)
{
  DmxMaster.maxChannel(relays_count());

  for (int i = 0; g_relays[i].id != -1; ++i)
  {
    g_relays[i].state = 0;
    DmxMaster.write(i, g_relays[i].state);
  }
}

void relays_update(void)
{
  for (int i = 0; g_relays[i].id != -1; ++i)
  {
    DmxMaster.write(g_relays[i].id, g_relays[i].state);
  }
}

void relay_set_state(int id, int state)
{
  g_relays[id].state = state;
}

int relay_get_state(int id)
{
  return g_relays[id].state;
}


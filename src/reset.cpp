
#include "reset.h"

#include "inputs.h"
#include "relays.h"

void reset(void)
{
  inputs_reset();
  relays_reset();
}



#ifndef   COMMANDS_H_
# define  COMMANDS_H_

# define  MAX_INPUTS 8
# define  MAX_RELAYS 8

typedef struct {
  int inputs[MAX_INPUTS];
  int relays[MAX_RELAYS];
  int end;
} Command;

void commands_process(void);

#endif


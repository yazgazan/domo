
#ifndef   GLOBALS_H_
# define  GLOBALS_H_


#include "inputs.h"
#include "relays.h"
#include "commands.h"

extern Input g_inputs[];
extern Relay g_relays[];
extern Command g_commands[];

#endif



#include <Arduino.h>

#include "commands.h"
#include "inputs.h"
#include "relays.h"

#include "globals.h"

void commands_process(void)
{
  for (int i = 0; g_commands[i].end != -1; ++i)
  {
    int input_res = 0;

    for (int j = 0; g_commands[i].inputs[j] != -1; ++j)
    {
      Input const * input = input_get(g_commands[i].inputs[j]);

      if (j == 0)
      {
        input_res = input->state;
      }
      else
      {
        input_res ^= input->state;
      }
    }

    for (int k = 0; g_commands[i].relays[k] != -1; ++k)
    {
      relay_set_state(g_commands[i].relays[k], input_res);
    }
  }
}

